import  json
import getopt
import collections
import sys
import math
import requests

"""Couchbase Diagnostic Script

This module allow you to analyse a running couchbase cluster for poblems with nodes, and other performance issues,

Example:
    
    $python3 couchbase_diag.py -c couchbase01.hodor.bskyb.com -u Admin -p Password -z hour
    
    --------------------------------------------------
    Cluster Version: 4.5.1-2844-enterprise
    --------------------------------------------------
    Node Status: healthy ClusterMembership: active
    --------------------------------------------------
    couchbase01.hodor.bskyb.com:8091
    couchbase02.hodor.bskyb.com:8091
    couchbase03.hodor.bskyb.com:8091
    couchbase04.hodor.bskyb.com:8091
    
    --------------------------------------------------
    CLUSTER WARNINGS
    --------------------------------------------------
    WARNING: OOM Errors are occuring on bucket1
    WARNING: Disk Write Queue is > 0 items for bucket1
    WARNING: Disk Write Time is > 10ms for bucket1
    WARNING: There are docs in the internal replica queues for bucket1
    WARNING: There have been ejections from bucket1
    WARNING: Documents in the XDCR queue for bucket1
    
Todo:
    * Add ability to save output
    * Allow Zoom functionility to work over all metrics, currenly only Queus, ejection, Write times
    
"""

class CouchbaseStat():
    def __init__(self, host, user, password, bucket, stat, zoom='minute'):
        self._is_couchbase = False

        self._node_url = 'http' + '://' + host + ':8091/pools/default/buckets/' + bucket + '/stats/' + stat
        try:
            response = requests.get(self._node_url, auth=(user, password), params={'zoom':zoom})
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print(e)
            sys.exit(1)

        if response.status_code == 200:
            self._stat_data = json.loads(response.text)
        else:
            self.invalid_cluster(response.status_code)

    def invalid_cluster(self, status_code):
        print('Cant connect to cluster: Response Code ' + str(status_code))
        sys.exit(1)

    def return_stat_value(self, mode='avg'):

        stat_list = []

        for node in self._stat_data['nodeStats']:
            if mode == 'avg':
                stat_list.append(sum(self._stat_data['nodeStats'][node])/len(self._stat_data['nodeStats'][node]))
            elif mode == 'max':
                stat_list.append(max(self._stat_data['nodeStats'][node]))
            elif mode == 'min':
                stat_list.append(min(self._stat_data['nodeStats'][node]))

        if mode == 'avg':
            return sum(stat_list) / len(stat_list)
        if mode == 'max':
            return max(stat_list)
        if mode == 'min':
            return min(stat_list)

    def return_url(self):
        return self._node_url


class CouchbaseCluster():
    def __init__(self, host, user, password, zoom='minute'):
        self._is_couchbase = False
        self._host = host
        self._user = user
        self._password = password
        self._zoom = zoom

        self._node_url = 'http' + '://' + host + ':8091/pools/default'
        # headers = {'Authorisation':"Basic Administrator:couchbase"}
        # req = urllib.request.Request(self._node_url, data=None,headers=headers)
        # print(req)
        try:
            response = requests.get(self._node_url, auth=(user, password))
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print(e)
            sys.exit(1)

        if response.status_code == 200:
            self._node_data = json.loads(response.text)
            self._is_couchbase = True
        else:
            self.invalid_cluster(response.status_code)

        self._bucket_url = 'http' + '://' + host + ':8091/pools/default/buckets'

        response = requests.get(self._bucket_url, auth=(user, password))
        if response.status_code == 200:
            self._bucket_data = json.loads(response.text)
            self._is_couchbase = True
        else:
            self.invalid_cluster(response.status_code)

        self._bucket_details = []

    def invalid_cluster(self, status_code):
        print('Cant connect to cluster: Response Code ' + str(status_code))
        sys.exit(1)

    def is_couchbase(self):
        return self._is_couchbase

    def return_url(self):
        return self._node_url

    def return_node_data(self):

        self._nodes = self._node_data['nodes']

        self._node_list = []
        self._node_details = []

        for node in self._nodes:
            self._node_list.append({'hostname': node['hostname'], 'status': node['status'],
                                    'clusterMembership': node['clusterMembership']})

        return self._node_list

    def return_node_stats(self):

        self._nodes = self._node_data['nodes']

        self._node_details = []

        for node in self._nodes:
            self._node_details.append(
                {'hostname': node['hostname'], 'cpuusage': node['systemStats']['cpu_utilization_rate'],
                 'freemem': node['systemStats']['mem_free'], 'swapused': node['systemStats']['swap_used'],
                 'version': node['version']})

        return self._node_details

    def return_bucket_data(self):

        self._bucket_details = []

        for bucket in self._bucket_data:
            advanced_stats = self.build_advanced_stats(bucket)
            basic_stats = {'bucketname': bucket['name'],
                           'quotaused': math.ceil(bucket['basicStats']['quotaPercentUsed']),
                           'diskFetches': round(bucket['basicStats']['diskFetches'])}
            basic_stats.update(advanced_stats)
            self._bucket_details.append(basic_stats)

        return self._bucket_details

    def build_advanced_stats(self, bucket):

        stats_to_capture = {'ep_tmp_oom_errors': 'max', 'disk_write_queue': 'max', 'avg_disk_update_time': 'max',
                            'ep_dcp_replica_items_remaining': 'max', 'ep_num_value_ejects': 'max',
                            'ep_diskqueue_items': 'max', 'ep_dcp_xdcr_items_remaining': 'max', 'ep_item_commit_failed': 'max'}
        reported_stats = {}

        for stat in stats_to_capture:
            reported_stats.update({stat: CouchbaseStat(self._host, self._user, self._password, bucket['name'],
                                                       stat,self._zoom).return_stat_value(stats_to_capture[stat])})
        return reported_stats

    def return_disk_data(self):
        return self._node_data['storageTotals']['hdd']


class DisplayCouchbaseCluster():
    def __init__(self, cluster=None):


        self._disk_stats = cluster.return_disk_data()
        self._node_stats = cluster.return_node_stats()
        self._bucket_stats = cluster.return_bucket_data()
        node_dict = cluster.return_node_data()

        self._node_status = collections.defaultdict(list)
        self._node_membership = collections.defaultdict(list)
        self._advice = ''
        self._warnings = ''

        for node in node_dict:
            self._node_status[node['status']].append(node['hostname'])
            self._node_membership[node['clusterMembership']].append(node['hostname'])

    def return_formatted_nodes(self, status, cluster_membership):

        status_set = set(self._node_status[status])
        cluster_membership_set = set(self._node_membership[cluster_membership])

        node_list = status_set.intersection(cluster_membership_set)

        border = '-' * 50

        output = ''

        if len(node_list) > 0:
            output = 'Node Status: ' + status + ' ClusterMembership: ' + cluster_membership + '\n'
            output = output + border + '\n'
            for node in node_list:
                output = output + node + '\n'

            output = output + self.generate_warning(status, cluster_membership, len(node_list))
            self.generate_advice(status, cluster_membership, node_list)

        return output

    def generate_warning(self, status, cluster_membership, number_nodes):

        if number_nodes > 0:

            if status == 'healthy' and cluster_membership == 'active':
                return '\n'
            elif status == 'warmup':
                return '\nWARNING: There are ' + str(number_nodes) + ' node(s) warming up\n'
            elif status == 'healthy' and cluster_membership == 'inactiveFailed':
                return 'WARNING: There are ' + str(
                    number_nodes) + ' node(s) in the cluster that have not been recovered\n'
            elif status == 'healthy' and cluster_membership == 'inactiveAdded':
                return 'WARNING: There are ' + str(
                    number_nodes) + ' node(s) in the cluster that have been recovered but no rebalance has been run\n'
            elif status == 'unhealthy' and cluster_membership == 'active':
                return '\nWARNING: There are ' + str(
                    number_nodes) + ' node(s) in the cluster that have failed but not been failed over\n'
            elif status == 'unhealthy' and cluster_membership == 'inactiveFailed':
                return 'WARNING: There are ' + str(number_nodes) + ' node(s) in the cluster that have been failedover\n'
        else:
            return ' '

    def analyse_cluster_stats(self):

        if self._disk_stats:

            if self._disk_stats['used']/self._disk_stats['quotaTotal'] > 0.8:
                self._warnings = self._warnings + 'WARNING: Cluster Disk Space is low\n'

    def analyse_node_stats(self):
        for node in self._node_stats:
            hostname, port = node['hostname'].split(':')
            if node['cpuusage'] > 80:
                self._warnings = self._warnings + 'WARNING: High CPU on ' + hostname + '\n'
            if node['swapused'] > 10:
                self._warnings = self._warnings + 'WARNING: High Swap usage on ' + hostname + '\n'
            if node['freemem'] < 2 * 1024 * 1024 * 1024:
                self._warnings = self._warnings + 'WARNING: Low free memory on ' + hostname + '\n'

    def analyse_bucket_stats(self):
        quotaused = ''
        diskFetches = ''
        freemem = ''
        ep_tmp_oom_errors = ''
        for bucket in self._bucket_stats:
            bucketname = bucket['bucketname']
            quotaused = bucket.get('quotaused', 0)
            diskFetches = bucket.get('diskFetches', 0)
            freemem = bucket.get('freemem', 10000000000)
            ep_tmp_oom_errors = bucket.get('ep_tmp_oom_errors',0)
            disk_write_queue = bucket.get('disk_write_queue',0)
            avg_disk_update_time = bucket.get('avg_disk_update_time',0)
            ep_dcp_replica_items_remaining = bucket.get('ep_dcp_replica_items_remaining',0)
            ep_num_value_ejects = bucket.get('ep_num_value_ejects',0)
            ep_dcp_xdcr_items_remaining = bucket.get('ep_dcp_xdcr_items_remaining', 0)
            ep_item_commit_failed = bucket.get('ep_item_commit_failed', 0)

            if quotaused > 80:
                self._warnings = self._warnings + 'WARNING: Bucket Quota reached 80 or higher for ' + bucketname + '\n'
            if diskFetches > 0:
                self._warnings = self._warnings + 'WARNING: Couchbase is Reading from Disk for ' + bucketname + '\n'
            if freemem < 2 * 1024 * 1024 * 1024:
                self._warnings = self._warnings + 'WARNING: Low free memory on ' + bucketname + '\n'
            if ep_tmp_oom_errors > 10:
                self._warnings = self._warnings + 'WARNING: OOM Errors are occuring on '+ bucketname + '\n'
            if disk_write_queue > 10:
                self._warnings = self._warnings + 'WARNING: Disk Write Queue is > 0 items for '+ bucketname + '\n'
            if avg_disk_update_time > 10*1000:
                self._warnings = self._warnings + 'WARNING: Disk Write Time is > 10ms for ' + bucketname + '\n'
            if ep_dcp_replica_items_remaining > 100:
                self._warnings = self._warnings + 'WARNING: There are docs in the internal replica queues for ' + bucketname + '\n'
            if ep_num_value_ejects > 0:
                self._warnings = self._warnings + 'WARNING: There have been ejections from ' + bucketname + '\n'
            if ep_dcp_xdcr_items_remaining > 100:
                self._warnings = self._warnings + 'WARNING: Documents in the XDCR queue for ' + bucketname + '\n'
            if ep_item_commit_failed > 0:
                self._warnings = self._warnings + 'WARNING: Commit Errors on ' + bucketname + '\n'




    def generate_advice(self, status, cluster_membership, node_list):

        if len(node_list) > 0:

            if status == 'healthy' and cluster_membership == 'active':
                return '\n'
            elif status == 'unhealthy' and cluster_membership == 'active':
                for node in node_list:
                    hostname, port = node.split(':')
                    self._advice = self._advice + '\nFAILOVER Required - On any running node run couchbase-cli failover -c localhost:8091 -u Administrator -p password --server-failover=' + hostname + ' --force\n'
                return self._advice
            elif status == 'unhealthy' and cluster_membership == 'inactiveFailed':
                for node in node_list:
                    hostname, port = node.split(':')
                    self._advice = self._advice + '\nNODE DOWN and failed over. Restart Couchbase on ' + hostname + '\n'
                return self._advice
            elif status == 'healthy' and cluster_membership == 'inactiveFailed':
                self._advice = self._advice + 'RECOVERY REQUIRED On any running node run \n'
                for node in node_list:
                    hostname, port = node.split(':')
                    self._advice = self._advice + 'couchbase-cli server-readd -c localhost:8091 -u Administrator -p password --server-add=' + hostname + '\n'
                    self._advice = self._advice + 'couchbase-cli recover -c localhost:8091 -u Administrator -p password --server-recovery=' + hostname + ' --recovery-type=delta\n'
                return self._advice
            elif status == 'healthy' and cluster_membership == 'inactiveAdded':
                self._advice = self._advice + 'REBALANCE REQUIRED On any running node run\n'
                self._advice = self._advice + 'couchbase-cli rebalance -c localhost:8091 -u Administrator -p password'
                return self._advice
            else:
                return self._advice
        else:
            return self._advice

    def build_report_header(self):

        version_set = set()
        version = ''

        for node in self._node_stats:
            version_set.add('Cluster Version: ' + node['version'] + '\n')

        for i in version_set:
            version = version + i

        if len(version_set) > 1:
            self._advice = self._advice + 'WARNING MIXED VERSIONS: Complete Cluster upgrade\n'

        return version

    def node_status_report(self):
        self.analyse_cluster_stats()
        self.analyse_node_stats()
        self.analyse_bucket_stats()
        border = '-' * 50 + '\n'
        report = border
        report = report + self.build_report_header()
        report = report + border
        report = report + self.return_formatted_nodes('healthy', 'active')
        report = report + self.return_formatted_nodes('warmup', 'active')
        report = report + self.return_formatted_nodes('unhealthy', 'active')
        report = report + self.return_formatted_nodes('unhealthy', 'inactiveFailed')
        report = report + self.return_formatted_nodes('healthy', 'inactiveFailed')
        report = report + self.return_formatted_nodes('healthy', 'inactiveAdded')
        if len(self._advice) > 1:
            report = report + border
            report = report + 'RECOVERY ACTIONS\n'
            report = report + border

        report = report + self._advice

        if len(self._warnings) > 1:
            report = report + border
            report = report + 'CLUSTER WARNINGS\n'
            report = report + border

        report = report + self._warnings

        print(report)

        return report

def usage(zoom=False):
    if not zoom:
        print('USAGE: couchbase_diag.py -c <hostname> -u <username> -p <password> -z <window>')
    else:
        print('ERROR: zoom param must be in minute, hour, day, month, year')


def main(argv):
    cluster = 'localhost'
    password = 'password'
    user = 'user'
    zoom = 'minute'
    try:
        opts, args = getopt.getopt(argv, "c:u:p:z:", ["cluster=", "user=", "password="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-c', '--cluster'):
            cluster = arg
        if opt in ('-u', '--user'):
            user = arg
        if opt in ('-p', '--password'):
            password = arg
        if opt in ('-z', '--zoom'):
            zoom = arg

    if zoom not in ('minute', 'hour', 'day', 'month', 'year'):
        usage(zoom=True)
        sys.exit(2)
    cluster = CouchbaseCluster(cluster, user, password, zoom)
    displaycluster = DisplayCouchbaseCluster(cluster)
    displaycluster.node_status_report()


if __name__ == "__main__":
    main(sys.argv[1:])
