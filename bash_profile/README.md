Couchbase Bash Profile
----------------
Sets up Couchbase environment, and access to additional couchbase tools to manage your cluster

Releases:
---------
  * v1.0 - Initial release
  
Requirements 
------------
Python 3.6.1

```pip install requests```

Install
-------

```
cd ~
git clone https://ianmerrington@bitbucket.org/ianmerrington/couchbase-management.git```


```

Tools
-----

* cb_node_status List status off all nodes, and provides diagnostics and commands to recover cluster
* cb_bucket_list List all buckets
* cb_xdcr_list List xdcr config


Example:
-------    
    $cb_node_status
    
    --------------------------------------------------
    Cluster Version: 4.5.1-2844-enterprise
    --------------------------------------------------
    Node Status: healthy ClusterMembership: active
    --------------------------------------------------
    couchbase01.video.seenit.com:8091
    couchbase02.video.seenit.com:8091
    couchbase03.video.seenit.com:8091
    couchbase04.video.seenit.com:8091
    
    --------------------------------------------------
    CLUSTER WARNINGS
    --------------------------------------------------
    WARNING: OOM Errors are occuring on bucket1
    WARNING: Disk Write Queue is > 0 items for bucket1
    WARNING: Disk Write Time is > 10ms for bucket1
    WARNING: There are docs in the internal replica queues for bucket1
    WARNING: There have been ejections from bucket1
    WARNING: Documents in the XDCR queue for bucket1
    

