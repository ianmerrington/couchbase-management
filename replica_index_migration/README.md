Replica Index Migration
=================

Usecase
-------

Used to migrate the current GSI indexex to replica indexes, leveraging Couchbase 5.0s ability to create multiple indexes fomr on statement.

Process
-------

Create all the new indexes using the definitions in rep_def_<bucket>.out
Generate a build statement build_<bucket>.n1ql This needs to be manually executed
Generate a script to drop non replica indexes using drop_non_rep_indexes.sh
Manually run the generated scipt drop_non_rep_<bucket>.n1ql


Instructions
------------

```bash
git clone https://ianmerrington@bitbucket.org/ianmerrington/couchbase-management.git
cd couchbase-management/replica_index_migration

./create_rep_index.sh <bucket>
/opt/couchbase/bin/cbq -u ian -f build_<bucket>.n1ql 
./drop_non_rep_indexes.sh <bucket>
/opt/couchbase/bin/cbq -u ian -f drop_non_rep_<bucket>.n1ql
```

