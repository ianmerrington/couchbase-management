#!/bin/bash
bucket=$1

/opt/couchbase/bin/cbq -u ian -f rep_def_$bucket.out

/opt/couchbase/bin/cbq --script "select name||',' from system:indexes where keyspace_id = '$bucket' and state = 'deferred'" -u ian -q -o output.json
sed -i  1d output.json 
python n1ql_to_csv.py -i output.json > output.csv
sed -i '$ s/.$//' output.csv

echo "Build index on \`${bucket}\`(" > build_$bucket.n1ql
cat output.csv >> build_$bucket.n1ql
echo ');' >> build_$bucket.n1ql
