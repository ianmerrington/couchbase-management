#!/bin/bash
bucket=$1
/opt/couchbase/bin/cbq --script "select name||',' from system:indexes where keyspace_id = '$bucket' and name not like '%repind%'" -u ian -q -o output.json
sed -i  1d output.json 
python n1ql_to_csv.py -i output.json > drop_non_rep_$bucket.n1ql
sed -i 's/.$//' drop_non_rep_$bucket.n1ql
sed -i 's/$/;/' drop_non_rep_$bucket.n1ql
sed -i "s/^/drop index \`${bucket}\`./g" drop_non_rep_$bucket.n1ql

echo run /opt/couchbase/bin/cbq -u ian -f  drop_non_rep_$bucket.n1ql
