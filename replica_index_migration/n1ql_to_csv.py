#!/usr/bin/env python
import json,sys, getopt
__author__ = 'nixCraft'
# Store input and output file names
ifile=''
ofile=''
 
# Read command line args
myopts, args = getopt.getopt(sys.argv[1:],"i:o:")
 
###############################
# o == option
# a == argument passed to the o
###############################
for o, a in myopts:
    if o == '-i':
        ifile=a
    elif o == '-o':
        ofile=a
    else:
        print("Usage: %s -i input -o output" % sys.argv[0])
 
# Display input and output file name passed as the args




with open(ifile) as json_data:
    d = json.load(json_data)
header=""
row_data=""
for key in d["signature"].keys(): header = header +  key + ','
#print(header)
for row in d["results"]:
   row_data=""
   #for key in row.values(): 
   for a in d["signature"].keys():
      try:
         #print(row[a]+',')
         row_data = row_data + row[a] + ','
      except KeyError:
         #print(',')
         row_data = row_data + ','
   print(row_data[:-1])  
