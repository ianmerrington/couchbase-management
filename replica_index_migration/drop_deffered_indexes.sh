#!/bin/bash
/opt/couchbase/bin/cbq --script "select name||',' from system:indexes where keyspace_id = 'seenitstudio' and state = 'deferred'" -u ian -q -o output.json
sed -i  1d output.json 
python n1ql_to_csv.py -i output.json > output.csv
sed -i 's/.$//' output.csv
sed -i 's/$/;/' output.csv
sed -i 's/^/drop index seenitstudio./g' output.csv
